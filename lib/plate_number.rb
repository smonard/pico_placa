# frozen_string_literal: true

class PlateNumber
  attr_reader :last_digit

  def initialize(last_digit)
    @last_digit = last_digit
  end

  def self.parse(raw_plate)
    raise ArgumentError, 'Bad plate number' if /^[A-Z]{3}-[0-9]{3,4}$/.match(raw_plate).nil?

    PlateNumber.new raw_plate[-1].to_i
  end
end
