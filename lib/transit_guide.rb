# frozen_string_literal: true

require 'json'

class TransitGuide
  attr_reader :day_constraints, :blacklisted_timeframes

  def initialize(rules_path = 'transitRules.json')
    load_rules(rules_path)
  end

  def free_transit?(date)
    @holidays.include?(date) || date.saturday? || date.sunday?
  end

  private

  def load_rules(path)
    rules = JSON.parse(File.read(path), symbolize_names: true)
    @day_constraints = rules[:dayRestriction]
    @blacklisted_timeframes = rules[:blacklistedTimeframes]
    @holidays = rules[:holidays].map { |date| Date.parse date, '%d-%m-%Y' }
  end
end
