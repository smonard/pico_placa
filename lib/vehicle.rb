# frozen_string_literal: true

require 'transit_guide'
require 'plate_number'

class Vehicle
  def initialize(plate_number, transit_guide)
    @transit_guide = transit_guide
    @plate_number = plate_number
  end

  def can_road_at?(datetime)
    return true if @transit_guide.free_transit?(datetime.to_date) || out_of_restriction?(datetime)

    in_allowed_timeframe? datetime
  end

  private

  def out_of_restriction?(datetime)
    constraint = @transit_guide.day_constraints.find { |const| const[:day].eql? datetime.wday }
    !constraint&.dig(:digits)&.include?(@plate_number.last_digit)
  end

  def in_allowed_timeframe?(datetime)
    date_str = datetime.strftime('%d-%m-%Y')
    @transit_guide.blacklisted_timeframes.none? do |timeframe|
      init = DateTime.strptime("#{date_str}T#{timeframe[:begin]}", '%d-%m-%YT%H:%M')
      ending = DateTime.strptime("#{date_str}T#{timeframe[:end]}", '%d-%m-%YT%H:%M')
      datetime.between? init, ending
    end
  end
end
