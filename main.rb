#! /bin/ruby
# frozen_string_literal: true

$LOAD_PATH.unshift File.join(__dir__, 'lib')

require 'date'
require 'vehicle'
require 'transit_guide'
require 'plate_number'

input_plate = PlateNumber.parse ARGV[0]
input_date = DateTime.strptime ARGV[1], '%d-%m-%YT%H:%M'

vehicle = Vehicle.new input_plate, TransitGuide.new
output_message = vehicle.can_road_at?(input_date) ?
    'The vehicle can road freely.' : 'The vehicle cannot road at the time.'

puts output_message
