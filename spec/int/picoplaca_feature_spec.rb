# frozen_string_literal: true

require 'open3'

RSpec.describe 'PicoYPlacaRules' do
  context 'when valid arguments' do
    it 'says transit is allowed when plate number is not restricted that day' do
      main_file = './main.rb FDR-4511 10-9-2020T12:32'
      output = `#{main_file}`
      expect(output).to include 'The vehicle can road freely'
    end

    it 'says transit is allowed in unrestricted timeframe' do
      main_file = './main.rb FDR-4519 11-9-2020T12:32'
      output = `#{main_file}`
      expect(output).to include 'The vehicle can road freely'
    end

    it 'says transit is allowed on holidays' do
      main_file = './main.rb FDR-4510 1-1-2021T8:32'
      output = `#{main_file}`
      expect(output).to include 'The vehicle can road freely'
    end

    it 'says transit is allowed on weekends' do
      main_file = './main.rb FDR-4510 6-9-2021T8:32'
      output = `#{main_file}`
      expect(output).to include 'The vehicle can road freely'
    end

    it 'says transit is not allowed for a plate number on a specific day' do
      main_file = './main.rb FDR-4513 8-9-2020T8:32'
      output = `#{main_file}`
      expect(output).to include 'The vehicle cannot road at the time'
    end
  end

  context 'when invalid argument' do
    it 'shows error on invalid plate number' do
      main_file = './main.rb SSSS-44513 8-9-2021T8:32'
      Open3.popen3(main_file) do |_, _, stderr, _|
        expect(stderr.read).to include 'Bad plate number'
      end
    end

    it 'shows error on invalid date (even in format)' do
      main_file = './main.rb FDR-4513 2021-05-05T8:32'
      Open3.popen3(main_file) do |_, _, stderr, _|
        expect(stderr.read).to include 'invalid date'
      end
    end
  end
end
