# frozen_string_literal: true

require 'vehicle'
require 'date'
require 'securerandom'

RSpec.describe Vehicle do
  let(:transit_guide) { double('TransitGuide') }
  let(:plate_number) { double('PlateNumber') }
  let(:vehicle) { described_class.new plate_number, transit_guide }

  context 'when can road' do
    before do
      allow(transit_guide).to receive(:day_constraints).and_return([{ day: 5, digits: [9, 0] }])
      allow(plate_number).to receive(:last_digit).and_return((1..8).to_a[rand(8)])
    end
    it 'returns true when transit is free' do
      datetime = DateTime.new
      allow(transit_guide).to receive(:free_transit?).and_return true
      result = vehicle.can_road_at? datetime
      expect(result).to be_truthy
    end
    it 'returns true when transit is not free but plate number is not restricted' do
      datetime = DateTime.parse '18-9-2020T7:50', '%d-%m-%YT%H:%M'
      allow(transit_guide).to receive(:free_transit?).and_return false
      result = vehicle.can_road_at? datetime
      expect(result).to be_truthy
    end
    it 'returns true when transit is not free and plate number is restricted but is not within blacklisted timeframes' do
      selected_hour = (10..23).map { |hour| (0..59).map { |minute| "#{hour}:#{minute}" } }.flatten.sample
      datetime = DateTime.parse "18-9-2020T#{selected_hour}", '%d-%m-%YT%H:%M'
      timeframes = [{ begin: '0:00', end: '9:59' }]
      allow(transit_guide).to receive(:blacklisted_timeframes).and_return timeframes
      allow(transit_guide).to receive(:day_constraints).and_return([{ day: 5, digits: (0..9).to_a }])
      allow(transit_guide).to receive(:free_transit?).and_return false
      result = vehicle.can_road_at? datetime
      expect(result).to be_truthy
    end
  end
  context 'when cannot road' do
    it 'returns false when transit is not free and plate number is restricted running in blacklisted timeframe' do
      restriction = mock_transit_guide_constraints
      selected_hour = (0..1).map { |hour| (0..59).map { |minute| "#{hour}:#{minute}" } }.flatten.sample
      base_day = 6 # which is sunday so plus 1 => monday, plus 2 => tuesday, 3 => wednesday
      datetime = DateTime.parse "#{base_day + restriction[:day]}-9-2020T#{selected_hour}", '%d-%m-%YT%H:%M'
      allow(transit_guide).to receive(:blacklisted_timeframes).and_return [{ begin: '00:00', end: '01:59' }]
      allow(transit_guide).to receive(:free_transit?).and_return false
      allow(plate_number).to receive(:last_digit).and_return(restriction[:digits][rand(2)])
      result = vehicle.can_road_at? datetime
      expect(result).to be_falsey
    end

    def mock_transit_guide_constraints
      restrictions = (1..5).map { |n| { day: n, digits: [n * 2 - 1, (n * 2) % 10] } }
      allow(transit_guide).to receive(:day_constraints).and_return(restrictions)
      restrictions.sample
    end
  end
end
