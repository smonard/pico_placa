# frozen_string_literal: true

require 'plate_number'
require 'securerandom'

RSpec.describe PlateNumber do
  context 'when valid plate' do
    it "creates a Plate instance with the plate's (4 digits) last digit" do
      raw_plate = 'DFC-1542'
      plate = PlateNumber.parse(raw_plate)
      expect(plate).to be_instance_of PlateNumber
      expect(plate.last_digit).to eq 2
    end
    it "creates a Plate instance with the plate's (3 digits) last digit" do
      raw_plate = 'HBA-454'
      plate = PlateNumber.parse(raw_plate)
      expect(plate).to be_instance_of PlateNumber
      expect(plate.last_digit).to eq 4
    end
  end

  context 'when invalid plate' do
    it 'raises an argument error' do
      raw_plate = SecureRandom.alphanumeric
      expect { PlateNumber.parse raw_plate }.to raise_error ArgumentError, 'Bad plate number'
    end
  end
end
