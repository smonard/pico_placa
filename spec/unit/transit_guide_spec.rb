# frozen_string_literal: true

require 'date'
require 'transit_guide'

RSpec.describe TransitGuide do
  let(:transit_guide) { described_class.new 'spec/data/testRules.json' }

  context 'when free transit_guide is allowed' do
    it 'returns true for holidays' do
      holiday = DateTime.parse '1-1-2021', '%d-%m-%Y'
      result = transit_guide.free_transit? holiday
      expect(result).to be_truthy
    end
    it 'returns true for weekends' do
      weekend = DateTime.parse '6-9-2020', '%d-%m-%Y'
      result = transit_guide.free_transit? weekend
      expect(result).to be_truthy
    end
  end

  context 'when free transit_guide is not allowed' do
    it 'returns false for non-holiday weekdays' do
      weekday = DateTime.parse '9-9-2020', '%d-%m-%Y'
      result = transit_guide.free_transit? weekday
      expect(result).to be_falsey
    end
  end

  context 'when loading from file' do
    it 'returns day constraints retrieved from the specified file' do
      constraints = [{ day: 1, digits: [0, 2] }, { day: 2, digits: [5, 9] }]
      actual_constraints = transit_guide.day_constraints
      expect(actual_constraints).to match constraints
    end
    it 'returns the blacklisted timeframes retrieved from the specified file' do
      timeframes = [{ begin: '00:00', end: '01:00' }]
      actual_timeframes = transit_guide.blacklisted_timeframes
      expect(actual_timeframes).to match timeframes
    end
  end
end
