# Pico&Placa command line program

The program can determine whether or not a certain vehicle (given its plate number) is allowed to be on the road.

## Requirements

A Ruby interpreter is needed to execute the script. This program was built using the following Ruby version: `ruby 2.6.3p62 (2019-04-16 revision 67580) [x86_64-linux]`

No other libraries/gems are required to run the program. The main features were built using the standard Ruby library.

## Running tests

In order to run the test suite, RSpec is required. It can be installed by typing:
`gem install rspec`

To run the test suite, just type:
`rspec`

## Running the program

Within this repository (cloned locally) one can execute the script and send two arguments: the plate number and the date-time (in the following format: dd-mm-yyyyTHH:MM, according to the South-American standard).

For example:

`./main.rb HGB-5040 12-9-2020T17:44` or
`ruby main.rb HGB-5040 12-9-2020T17:44`

Where `main.rb` is the entry point for the script, `HGB-5040` is the plate number of a vehicle and `12-9-2020T17:44` is the date-time 

